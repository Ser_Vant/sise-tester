var provider = require('./driver.js');
var test = require('selenium-webdriver/testing');
var urlParser = require('url');
var _ = require('underscore');

var setConfig = function(conf){
    _.each(conf, function(param, paramName){
        if(this.__config.hasOwnProperty(paramName)) {
            this.__config[paramName] = param;
        }
    }.bind(this));
};

var Tester = function(data){ this.init(data); };

Tester.prototype = {
    init:function(data){
        setConfig.call(this, data);
    },
    __config:{
        protocol:'http',
        serverHost:null,
        port:80,
        httpBasicAuth:false, // {login:'somelogin', pass:'somepass'},
        serverAnswerTimeout:3000,
        searchDOMTimeout:100,

        // driver conf
        seleniumPort:null,
        usePopulate:false,
        populateScript:null,
        startWebServer:false,
        webServerScript:null,
        logServerOutput:false,
        logPath:null
    },
    __isFirst:true,
	__instance:null,
	test:test,
	promise:provider.promise,
    isFirst:function(){
        var v = this.__isFirst;
        if(this.__isFirst) {
            this.__isFirst = !this.__isFirst;
        }
        return v;
    },
	tearDown:function(){
		return new provider.promise.Promise(function(resolve, reject){
			if(this.__instance) {
				this.__instance.tearDown().then(resolve, reject);
			} else {resolve();}
		}.bind(this));
	},
	getProvider:function(){
		if(this.__instance === null) {
			this.__instance = (new provider.Provider()).setup(this.__config);
		}
		return this.__instance;
	},
	getDriver:function(){return this.getProvider().getDriver();},
    getServerHost:function(useBasicAuth){
        var r =  (this.__config.protocol || 'http')
        +'://'
        +(useBasicAuth && this.__config.httpBasicAuth?(this.__config.httpBasicAuth.login+':'+this.__config.httpBasicAuth.pass+'@'):'')
        +(this.__config.serverHost||'localhost')
        +':'+this.__config.port;
        return r;
    },
	goto:function(url, condition){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(driver){
				url = url.replace(this.getServerHost(), '');
				var gt = new provider.promise.Promise(function(resolveGT, rejectGT){
                    this.catchLogErrors().then(function(){
                        driver.navigate().to(this.getServerHost(this.isFirst())+url).then(resolveGT, rejectGT);
                    }.bind(this), reject);
                }.bind(this));
				if(condition) {
					gt.then(function(){
						this.waitFor(condition).then(resolve, reject);
					}.bind(this), reject);
				} else {
					gt.then(resolve, reject);
				}
			}.bind(this), reject);
		}.bind(this));
	},
	catchLogErrors:function(){
		return new provider.promise.Promise(function(resolve, reject){
			this.getLogs().then(function(logList){
				var lst = [];
				_.each(logList, function(log){
					lst.push(new provider.promise.Promise(function(resolveLg, rejectLg){
						// "OFF", "SEVERE", "WARNING", "INFO", "CONFIG", "FINE", "FINER", "FINEST", "ALL".
						if(log.level.name === 'SEVERE') {
							resolveLg(log.message);
						} else {
							resolveLg();
						}
					}));
				});
				provider.promise.all(lst).then(function(err){
					resolve(_.compact(err));
				}, reject);
			}.bind(this), reject);
		}.bind(this));
	},
	getLogs:function(){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.manage().logs().get('browser').then(resolve, reject);
			}, reject);
		}.bind(this));
	},
	getElement:function(xpath){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.findElement(provider.By.xpath(xpath)).then(function(element){ resolve(element);}, reject);
			}.bind(this), reject);
		}.bind(this));
	},
	getElements:function(xpath){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.findElements(provider.By.xpath(xpath)).then(function(element){ resolve(element);}, reject);
			}.bind(this), reject);
		}.bind(this));
	},
	input:function(xpath, value){
		return new provider.promise.Promise(function(resolve, reject){
			this.waitForElement(xpath).then(function(element){
				element.sendKeys(value).then(function(){ resolve(element);}, reject);
			}.bind(this), reject);
		}.bind(this));
	},
	waitFor:function(condition, waitForDOM){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.wait(condition, waitForDOM && this.__config.searchDOMTimeout || this.__config.serverAnswerTimeout).then(function(element){
                    resolve(element);
                }, reject);
			}.bind(this), reject);
		}.bind(this));
	},
	click:function(xpath, condition){
		return new provider.promise.Promise(function(resolve, reject){
			this.waitForElement(xpath).then(function(element){
				var clk = element.click();
				if(condition) {
					clk.then(function(){
						this.waitFor(condition).then(function(){resolve(element);}, reject);
					}.bind(this), reject);
				} else {
					clk.then(function(){ resolve(element);}, reject);
				}
			}.bind(this), reject);
		}.bind(this));
	},
	untilElementTextContains:function(xpath, text){
		return new provider.promise.Promise(function(resolve, reject){
			this.waitForElement(xpath).then(function(element){
				provider.until.elementTextContains(element, text).then(resolve, reject);
			}.bind(this), reject);
		}.bind(this));
	},
	waitForElementText:function(xpath, text, useDefaultTO){
		return new provider.promise.Promise(function(resolve, reject){
			this.waitForElement(xpath, useDefaultTO).then(function(element){
				this.waitFor(provider.until.elementTextContains(element, text)).then(resolve, reject);
			}.bind(this));
		}.bind(this));
	},
	waitForElement:function(xpath, useDefaultTO){
		return new provider.promise.Promise(function(resolve, reject){
			this.waitFor(this.untilElement(xpath), useDefaultTO!==true).then(function(){
                this.getElement(xpath).then(function(element){
                    resolve(element);
                }, reject);
            }.bind(this), reject);
		}.bind(this));
	},
    untilTitle:function(title){return provider.until.titleIs(title);},
    untilElement:function(xpath){return provider.until.elementsLocated(provider.By.xpath(xpath));},
    assertValue:function(xpath, value){
        return new provider.promise.Promise(function(resolve, reject){
            this.waitForElement(xpath).then(function(element){
                element.getTagName().then(function(tag){
                    var pr = null;
                    if(tag === 'input' || tag === 'textarea') {
                        pr = new provider.promise.Promise(function(resolveInp, rejectInp){
                            element.getAttribute('value').then(function(realVal){
                                if(realVal === value) {
                                    resolveInp();
                                } else {
                                    rejectInp();
                                }
                            }.bind(this), rejectInp);
                        }.bind(this));
                    } else {
                        pr = provider.promise.rejected(new Error('Unrecognized tag name "'+tag+'"'));
                    }
                    pr.then(resolve, reject);
                }.bind(this), reject);
            }, reject);
        }.bind(this));
    },
    assertElementText:function(xpath, value){
        return new provider.promise.Promise(function(resolve, reject){
            this.waitForElement(xpath).then(function(element){
                element.getText().then(function(realValue){
                    if(realValue === value) {
                        resolve();
                    } else {
                        reject(new Error('Text contents assertion error '+realValue+' - '+value));
                    }
                }.bind(this), reject);
            }.bind(this), reject);
        }.bind(this));
    },
    wait:function(to){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.sleep(to);
                resolve();
			}, reject);
		}.bind(this));
    },
    waitForReload:function(){
		return this.wait(500);
    },
    getJSData:function(script, sc_args){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.executeScript(script, sc_args).then(resolve, reject);
			}, reject);
		}.bind(this));
    },
    getURL:function(){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
				drv.getCurrentUrl().then(function(url){
                    var curPathParsed = urlParser.parse(url);
                    resolve(curPathParsed.path+curPathParsed.hash||'');
                }, reject);
			}, reject);
		}.bind(this));
    },
    waitForUrlChange:function(from){
		return new provider.promise.Promise(function(resolve, reject){
			this.getDriver().then(function(drv){
                var to = null;
                var toRepeat = null;
				var check = function(){
                    drv.getCurrentUrl().then(function(url){
                        var curPathParsed = urlParser.parse(url);
                        var curPath = curPathParsed.path+curPathParsed.hash||'';
                        if(!from) {
                            from = curPath;
                        }
                        if(from !== curPath) {
                            if(to) { clearTimeout(to); }
                            resolve(curPath);
                        } else {
                            if(toRepeat) {clearTimeout(toRepeat);}
                            toRepeat = setTimeout(check.bind(this), 50);
                        }
                    }.bind(this), reject);
                };
                to = setTimeout(function(){
                    reject(new Error('URL is not changed from '+from+' '+(this.__config.serverAnswerTimeout/1000)+'s'));
                }.bind(this), this.__config.serverAnswerTimeout);
                check.apply(this);
			}.bind(this), reject);
		}.bind(this));
    },
    any:function(promises, errorText){
        return new provider.promise.Promise(function(resolve, reject){
            var cancelAll = function(callback){
                _.each(promises, function(promise){
                    promise.state_ === 'pending' && promise.cancel();
                });
                callback();
            };
            _.each(promises, function(promise, ix){
                promise.then(function(){
                    var res = _.find(promises, function(promiseItem){
                        return promiseItem.state_ === 'fulfilled';
                    });
                    if(res) {
                        cancelAll(function(){
                            resolve({
                                index: ix,
                                promise: res
                            });
                        });
                    }
                }, function(){
                    cancelAll(function(){
                        reject(new Error(errorText||'All of promises are rejected'));
                    });
                });
            });
        }.bind(this));
    }
};
var __instance = null;
exports.Tester = Tester;
exports.setConfig = function(data){
    if(!__instance) {
        __instance = new Tester(data);
    }
    return __instance;
};
exports.getInstance = function(){ return __instance; };

exports._ = _;