var seleniumInstaller = require('selenium-standalone');
var driver = require('selenium-webdriver');
//var test = require('selenium-webdriver/testing');
var chrome = require('selenium-webdriver/chrome');
//var pathchrome = require('chromedriver').path;
var net = require('selenium-webdriver/net');
var url = require('url');

var promise = driver.promise;
var By = driver.By;
var until = driver.until;
var spawn = require('child_process').spawn;
var shell = require('shelljs');
var fs = require('fs');
var mkdirp = require('mkdirp');
var _ = require('underscore');

var drivers = {
    chrome: {
        version: '2.9',
        arch: process.arch,
        baseURL: 'http://chromedriver.storage.googleapis.com'
    }
};

function DriverProvider(){
	this.__config = {
        seleniumPort:null,
        usePopulate:false,
        populateScript:null,
        startWebServer:false,
        webServerScript:null,
        logServerOutput:false,
        logPath:null
    };
	this.__prStarted = null;
	this.__prDriver = null;
	this.__driver = null;
	this.__seleniumServer = null;
	this.__webServer = null;
    this.__webServerCommand = null;
}
DriverProvider.prototype = {
    setup:function(conf){
        _.each(conf, function(param, paramName){
            if(this.__config.hasOwnProperty(paramName)) {
                this.__config[paramName] = param;
            }
        }.bind(this));
        return this;
    },
	installSeleniumServer:function(){
        return new promise.Promise(function(resolve, reject){
            seleniumInstaller.install({
                version: '2.46.0',
                baseURL: 'http://selenium-release.storage.googleapis.com',
                drivers: drivers,
                logger: function(message) {},
                progressCb: function(totalLength, progressLength, chunkLength) {
                    console.log(progressLength+' of '+totalLength);
                }
            }, function(){
				resolve();
			});
        }.bind(this));
	},
	startSeleniumServer:function(){
		return new promise.Promise(function(resolve, reject){
            var startOptions = {
                drivers: drivers,
                seleniumArgs:['-port '+this.__config.seleniumPort]
            };
            seleniumInstaller.start(startOptions, function(err, child){
                this.__seleniumServer = child;

                if(err) {
                    reject();
                } else {
                    resolve();
                }
            }.bind(this));
        }.bind(this));
	},
    prepareDB:function(){
		return new promise.Promise(function(resolve, reject){
            if(this.__config.usePopulate && this.__config.populateScript) {
                var script = this.__config.populateScript.command + ' ' + this.__config.populateScript.path;
                shell.exec(script, {async:true, silent:true}, function(code, output){
                    if(code>0) {
                        reject();
                    } else {
                        resolve();
                    }
                });
            } else {
                resolve();
            }
        }.bind(this));
    },
    getSiblingProcess:function(){
		return new promise.Promise(function(resolve, reject){
            if(this.__webServerCommand) {
                setTimeout(function(){
                    shell.exec('ps ax | grep "'+this.__webServerCommand+'" | grep -v grep | awk \'{print($1)}\'', {async:true, silent:true}, function(code, output){
                        if(code>0) {
                            reject();
                        } else {
                            resolve(parseInt(output));
                        }
                    });
                }.bind(this), 1000);
            } else {resolve(null);}
        }.bind(this));
    },
    getPPID:function(pid, file){
		return new promise.Promise(function(resolve, reject){
            shell.exec('ps --no-headers -o "%P" -p '+pid, {async:true, silent:true}, function(code, output){
                console.log(parseInt(output), process.pid, pid)
                var realPid = parseInt(output) !== parseInt(process.pid) ? parseInt(output) : pid;
                if(code>0) {
                    reject();
                } else {
                    resolve(realPid);
                }
            });
        }.bind(this));
    },
	startWebServer:function(){
		return new promise.Promise(function(resolve, reject){
            if(this.__config.startWebServer && this.__config.webServerScript) {
                var command = this.__config.webServerScript.command;
                var arguments = this.__config.webServerScript.additional;
                var scriptName = this.__config.webServerScript.path;
                this.__webServer = spawn(command, [scriptName, arguments]);
                if(this.__config.logServerOutput && this.__config.logPath) {
                    var logDir = this.__config.logPath.path;
                    mkdirp(logDir, function(err) {
                        this.__webServer.stderr.on('data', function(data) {
                            fs.appendFile(logDir+'/err.log', data);
                        });
                        this.__webServer.stdout.on('data', function(data) {
                            fs.appendFile(logDir+'/out.log', data);
                        });
                    }.bind(this));
                }
                this.__webServer.on('error', function(err){ reject(err); });
                this.__webServerCommand = command+' '+scriptName+' '+arguments;
            }
            resolve();
        }.bind(this));
	},
	stopSeleniumServer:function(){
		if(this.__seleniumServer) {
			this.__seleniumServer.kill();
		}
		return promise.fulfilled(true);
	},
	stopWebServer:function(){
        return new promise.Promise(function(resolve, reject){
            if(this.__webServer) {
                this.__webServer.kill();
                this.getSiblingProcess(this.__webServerCommand).then(function(siblingID){
                    shell.exec('kill '+siblingID, {async:true, silent:true}, function(code, output){
                        if(code>0) {
                            reject();
                        } else {
                            resolve(parseInt(output));
                        }
                    });
                }.bind(this), reject);
            } else {
                resolve(false);
            }
		}.bind(this));
	},
	startUp:function(){
		if(!this.__prStarted) {
			var flow = promise.controlFlow();
			this.__prStarted = promise.all([
				flow.execute(this.installSeleniumServer.bind(this)),
				flow.execute(this.startSeleniumServer.bind(this)),
				flow.execute(this.prepareDB.bind(this)),
				flow.execute(this.startWebServer.bind(this))
			]);
		}
		return this.__prStarted;
	},
	destroyDriver:function(){
		return this.__driver ?
			this.__driver.quit() :
			promise.rejected(new Error('No driver was found'));
	},
	tearDown:function(){
		if(this.__prStarted) {
			var flow = promise.controlFlow();
			return promise.all([
				flow.execute(this.destroyDriver.bind(this)),
				flow.execute(this.stopSeleniumServer.bind(this)),
				flow.execute(this.stopWebServer.bind(this))
			]);
		} else {
			return promise.fulfilled(true);
		}
	},
    getSeleniumUrl:function(){
        return url.format({
            protocol: 'http',
            hostname: net.getLoopbackAddress(),
            port: this.__config.seleniumPort,
            pathname: '/wd/hub'
        });
    },
	getDriver:function(){
		return new promise.Promise(function(resolve, reject){
			this.startUp().then(function(){
				if(this.__prDriver === null) {
					this.__prDriver = new promise.Promise(function(resolveDriver, rejectDriver){
						var capability = new driver.Capabilities(driver.Capabilities.chrome());
						capability.setLoggingPrefs({
							driver: "ALL",
                            server: "OFF",
                            browser: "ALL"
						});
						this.__driver = new driver.Builder()
//							.withCapabilities(capability)
                            .usingServer(this.getSeleniumUrl())
							.forBrowser(driver.Browser.CHROME)
							.build();
                        this.__driver.manage().window().setSize(1920, 1080);
						if (!this.__driver) {
							return promise.rejected(new Error('Unable to build driver'));
						} else {
							resolveDriver(this.__driver);
						}
					}.bind(this));
				}
				this.__prDriver.then(function(drv){resolve(drv);}, reject);
			}.bind(this), reject);
		}.bind(this));
	}
};

module.exports = {
	Provider:DriverProvider,
//	test:test,
	promise:promise,
	By:By,
	until:until
};
